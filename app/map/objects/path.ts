import { Difficulty } from "~/map/objects/difficulty";
import { Point } from "~/map/objects/point";
import { Waypoint } from "~/map/objects/waypoint";

export class Path {

    id: number;

    /** Minutes */
    walkingTime: number;

    cyclingTime: number;

    name: string;

    points: Array<Point>;

    category: string;

    difficulty: Difficulty;

    waypoint: Array<Waypoint>;

}
