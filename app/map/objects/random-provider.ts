import { Path, Place, Point } from "./objects";
import { Waypoint } from "./waypoint";

export function createWaypoints(amount: number = 30, startingPoint = new Point()): Array<Waypoint> {
    const waypoints: Array<Waypoint> = [];

    for (let i = 0; i < amount; ++i) {
        const x = (Math.random() - 0.5) / 5;
        const y = (Math.random() - 0.5) / 5;
        const waypoint = new Waypoint();
        waypoint.location = new Point(startingPoint.x + x, startingPoint.y + y);
        waypoints.push(waypoint);
    }

    return waypoints;
}

export function createPath(startingPoint = new Point()): Path {
    const points: Array<Point> = [];
    let lastPoint = startingPoint;
    points.push(startingPoint);

    let shiftX = 0.0;
    let shiftY = 0.0;

    for (let i = 0; i < 400; ++i) {
        const diffX = (Math.random() - 0.5) / 1000;
        const diffY = (Math.random() - 0.5) / 1000;
        shiftX += diffX;
        shiftY += diffY;
        const newPoint = new Point(lastPoint.x + shiftX, lastPoint.y + shiftY);
        points.push(newPoint);
        lastPoint = newPoint;
    }
    const path = new Path();
    path.points = points;

    return path;
}
export function createPolygon(startingPoint = new Point()): Array<Point> {
    const points: Array<Point> = [];
    let lastPoint = startingPoint;

    for (let i = 0; i < 30; ++i) {
        const diffX = (Math.random() - 0.5) / 100;
        const diffY = (Math.random() - 0.5) / 100;
        const newPoint = new Point(lastPoint.x + diffX, lastPoint.y + diffY);
        points.push(newPoint);
        lastPoint = newPoint;
    }
    
    return points;
}
