export class Place {

    id: number;
    icon: string;
    title: string;
    description: string;
    shortDescription: string;

}
