import { MapboxView, SetCenterOptions, SetZoomLevelOptions } from "nativescript-mapbox";
import { Color, EventData, Page } from "tns-core-modules/ui/page/page";
import { Point } from "../objects/point";
import { IMap } from "./../map";
import { MapboxMap } from "./../mapbox-map";
import { createPath, createPolygon, createWaypoints } from "./../objects/random-provider";

let page: Page;

export function onLoaded(args: EventData) {
    page = args.object as Page;
    page.actionBarHidden = true;
}

export function onMapReady(args) {
    const mapView = args.map as MapboxView;

    const location = new Point(19.1389801, 49.706649);

    const waypoints = createWaypoints(30, location);
    const map: IMap = new MapboxMap(mapView);

    console.log("What is happening? :f");

    map.setLocation(location.x, location.y);
    map.zoom = 12;

    for (const waypoint of waypoints) {
        map.addWaypoint(waypoint);
    }

    for (let i = 0; i < 2; ++i) {
        const path = createPath(location);
        map.addPath(path, new Color("#00FFFF"));
    }

    const points = [            [
        19.228134155273438,
        49.66740561656091
      ],
      [
        19.248046875,
        49.67607090084366
      ],
      [
        19.248390197753906,
        49.687622211880765
      ],
      [
        19.24358367919922,
        49.706054195660606
      ],
      [
        19.239463806152344,
        49.71959613648118
      ],
      [
        19.230880737304688,
        49.73779410787209
      ],
      [
        19.221954345703125,
        49.74910891737842
      ],
      [
        19.213714599609375,
        49.75132719804583
      ],
      [
        19.200668334960938,
        49.75199266246325
      ],
      [
        19.189682006835934,
        49.74733421979961
      ],
      [
        19.182472229003906,
        49.73735033608063
      ],
      [
        19.174575805664062,
        49.73335620733158
      ],
      [
        19.158782958984375,
        49.72891790094871
      ],
      [
        19.149513244628906,
        49.72048400068801
      ],
      [
        19.14882659912109,
        49.71116061678412
      ],
      [
        19.129600524902344,
        49.706054195660606
      ],
      [
        19.109344482421875,
        49.695617665795524
      ],
      [
        19.10625457763672,
        49.68118047304402
      ],
      [
        19.13166046142578,
        49.66229459701699
      ],
      [
        19.182472229003906,
        49.65362685844596
      ],
      [
        19.228134155273438,
        49.66740561656091
      ]].map((value) => new Point(value[0], value[1]));
    map.addPolygon(points, new Color("#00FF55"));

}
