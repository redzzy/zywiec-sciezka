import { Color } from "tns-core-modules/color/color";
import { Path, Point, Waypoint } from "~/map/objects/objects";

type Area = Path;

export interface IMap {

    zoom: number;

    useAnimations: boolean;

    /** Clears all the waypoints and paths on the map */
    clear();

    addWaypoint(waypoint: Waypoint);

    addPath(path: Path, color?: Color);

    addPolygon(points: Array<Point>, color?: Color);

    setLocation(x: number, y: number);

}
