import {
    AddPolygonOptions, AddPolylineOptions, LatLng,
    MapboxMarker, MapboxView, SetCenterOptions, SetZoomLevelOptions
} from "nativescript-mapbox";
import { Color } from "tns-core-modules/color/color";
import { IMap } from "~/map/map";
import { Path, Point, Waypoint } from "~/map/objects/objects";

export class MapboxMap implements IMap {

    useAnimations: boolean = true;

    /** Required, because MapboxView.getZoomLevel() is async */
    private zoomLevel: number;
    private polylinesIds = [];
    private waypointsIds = [];

    constructor(public mapbox: MapboxView) {
        this.mapbox.getZoomLevel().then((zoom) => {
            this.zoomLevel = zoom;
        });
    }

    addPath(path: Path, color?: Color) {
        // Registers and add ID
        const id = this.polylinesIds.length;
        this.polylinesIds.push(id);

        const mapPoints = path.points.map(toLongLat);
        const polylineOptions = <AddPolylineOptions>{
            id,
            width: 5,
            color,
            points: mapPoints
        };
        this.mapbox.addPolyline(polylineOptions);

        // Add waypoints
        if (path.waypoint) {
            for (const waypoint of path.waypoint) {
                this.addWaypoint(waypoint);
            }
        }
    }

    addWaypoint(waypoint: Waypoint) {
        const id = this.waypointsIds.length;
        this.waypointsIds.push(id);

        const location = toLongLat(waypoint.location);
        const marker = <MapboxMarker>{
            lng: location.lng,
            lat: location.lat
        };
        if (waypoint.place != null) {
            marker.subtitle = waypoint.place.shortDescription;
            marker.title = waypoint.place.title;
        }

        this.mapbox.addMarkers([marker]);
    }

    clear() {
        this.mapbox.removePolylines(this.polylinesIds);
        this.mapbox.removeMarkers(this.waypointsIds);
    }

    // tslint:disable-next-line:no-empty
    addPolygon(points: Array<Point>, color?: Color) {
        this.mapbox.addPolygon({
            fillColor: color,
            fillOpacity: 0.5,
            points: points.map((point) => ({ lng: point.x, lat: point.y } as LatLng))
        } as AddPolygonOptions);
    }

    setLocation(x: number, y: number) {
        this.mapbox.setCenter({
            lng: x,
            lat: y,
            animated: this.useAnimations
        } as SetCenterOptions);
    }

    set zoom(value: number) {
        this.mapbox.setZoomLevel({
            animated: this.useAnimations,
            level: value
        } as SetZoomLevelOptions);
        this.zoomLevel = value;
    }

    get zoom(): number {
        return this.zoomLevel;
    }

}

function toLongLat(point: Point): LatLng {
    return {
        lng: point.x,
        lat: point.y
    } as LatLng;
}
