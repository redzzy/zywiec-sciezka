import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { EventData, Observable, Page } from "tns-core-modules/ui/page/page";

let page: Page;

export function onLoaded(args: EventData) {
    page = args.object as Page;
    page.actionBarHidden = true;
    page.bindingContext = new PathsPageModel();
    page.getViewById("scroller").on("scroll", onScroll);
}

class PathsPageModel extends Observable {

    constructor() {
        super();
        this.set("title", "Trasy dydaktyczne");
        this.set("types", new ObservableArray([
            "Spacer",
            "Bieg",
            "Rower",
        ]));
        this.set("repeat", Array(30).fill(null));
    }

}

// tslint:disable-next-line:no-empty
export function onScroll(args) { }
