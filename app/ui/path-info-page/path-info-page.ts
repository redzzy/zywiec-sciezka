import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { PanGestureEventData, TouchGestureEventData } from "tns-core-modules/ui/gestures/gestures";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout/stack-layout";
import { EventData, Observable, Page, translateXProperty } from "tns-core-modules/ui/page/page";
import { ScrollView } from "tns-core-modules/ui/scroll-view/scroll-view";

let page: Page;
// let contentScroller: ScrollView;
let content: StackLayout;

// tslint:disable-next-line:prefer-const
let isScrollingEnabled: boolean = true;

// Controls the scroll
let y = 0.0;

// Minimum Y the content is allowed to go to
let minY;

export function onLoaded(args: EventData) {
    page = args.object as Page;
    page.actionBarHidden = true;
    page.bindingContext = new PathInfoPageModel();

    // contentScroller = page.getViewById("contentScroller");
    content = page.getViewById("content");

    initialize();
}

declare let android: any;

function initialize() {
    y = 0;
    minY = content.originY;
    content.on("pan", onContentPan);
    content.on("touch", onContentTouch);
}

// Behold! The legacy code.
// function initialize() {
//     const child = contentScroller.getViewById("contentScrollerChild");
//     contentScroller.scrollBarIndicatorVisible = false;

//     contentScroller.on("scroll", onContentScroll);
//     contentScroller.on("pan", onContentPan);
//     const on = new android.view.View.OnTouchListener({
//         onTouch(): boolean {
//             return isScrollingEnabled;
//         }
//     });
//     contentScroller.android.setOnTouchListener(on);
// }

function onContentTouch(args: TouchGestureEventData) {
    if (args.action === "down") {
        y = content.translateY;
    }
}

function onContentPan(args: PanGestureEventData) {
    content.translateY = y + args.deltaY;
    // So the view won't go below its origin
    if (content.translateY > 0) {
        content.translateY = 0;
    }
}

function onContentScroll(args) {
    // console.log(args);
}

class PathInfoPageModel extends Observable {

    constructor() {
        super();
    }

}
